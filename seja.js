// Za združljivost razvoja na lokalnem računalniku ali v Cloud9 okolju
if (!process.env.PORT) {
  process.env.PORT = 8080;
}

var express = require("express");
var streznik = express();

// Podpora sejam na strežniku
var expressSession = require("express-session");
streznik.use(
  expressSession({
    secret: "123456789QWERTY",    // Skrivni ključ za podpisovanje piškotov
    saveUninitialized: true,      // Novo sejo shranimo
    resave: false,                // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 2000                // Seja poteče po 2s neaktivnost
    }
  })
);

var stDostopov = 0;

streznik.get("/", function(zahteva, odgovor) {
  stDostopov++;
  if (!zahteva.session.stDostopov) {
    zahteva.session.stDostopov = 1;
  } else {
    zahteva.session.stDostopov++;
  }
  odgovor.send(
    "<h1>Globalne spremenljivke vs. seja</h1>" +
    "<p style='font-size:150%'>" +
    "To je že <strong>" + stDostopov + "x</strong> dostop do strežnika," +
    "medtem ko ste v tej seji dostopali <strong>" +
    zahteva.session.stDostopov + "x<strong>." +
    "</p>"
  );
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik je pognan!");
});